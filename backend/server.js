import path from 'path';
import express from 'express';
import connectDB from './config/db.js'

const app = express();
connectDB();

const PORT = process.env.PORT || 5000
require("./models/user");
require("./models/post");

app.use(express.json());
app.use(require("./routes/auth"));
app.use(require("./routes/post"));
app.use(require("./routes/user"));

if (process.env.NODE_ENV == "production") {
    app.use(express.static("client/build"));
    const path = require("path");
    app.get("*", (req, res) => {
        res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
    });
}


app.listen(PORT, () => {
    console.log(`Server started at port ${PORT}`)
})